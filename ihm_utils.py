from datetime import datetime
import locale
import pathlib
import os

    
STATUS = {
    0: 'Éteint',
    1: 'Arrêté',                          # Jamais vu
    2: "En cours d'allumage - Vérification",
    3: "En cours d'allumage - Chargement granulés",
    4: 'Allumage',
    5: 'Allumage - Contrôle combustion',
    6: 'Allumé',
    9: 'En veille - il fait assez chaud',
    10: "En cours d'extinction",
    11: 'Nettoyage',                      # Jamais vu
    12: 'Refroidissement',                # Jamais vu
    241: 'Erreur nettoyage',              # Jamais vu
    243: 'Erreur Grille',                 # Jamais vu
    244: 'NTC2 ALARM',                    # Jamais vu
    245: 'NTC3 ALARM',                    # Jamais vu
    247: 'Erreur Porte',                  # Jamais vu
    248: 'Erreur Dépression',             # Jamais vu
    249: 'NTC1 ALARM',                    # Jamais vu
    250: 'TC1 ALARM',                     # Jamais vu
    252: 'Erreur évacuation Fumée',       # Jamais vu
    253: "Il n'y a plus de granulés"
}

"""
Le poêle est allumé :
"""
ON_STATUS = [4, 5, 6]
"""
Le poêle est éteint :
"""
OFF_STATUS = [0, 10, 253]


web_content_directory = os.path.join(pathlib.Path(__file__).parent.resolve(), 'docs')

"""
Les niveaux de debug :
"""
DEBUG = 1
INFO = 2
WARN = 3
ERROR = 4

LEVEL = {
    DEBUG: 'DEBUG',
    INFO : 'INFO ',
    WARN : 'WARN ',
    ERROR: 'ERROR'
}

LOG_LEVEL = DEBUG

def charge_fichier_html(nom_fichier, options={}):
    """
    Permet de charger le contenu d'un fichier HTML.

    Paramètres :
        - nom_fichier : Le nom du fichier à charger.
        - message : Un message éventuel à afficher à l'utilisateur.
    """
    html_skel = open(web_content_directory + "/" + nom_fichier,
                     'r',
                     encoding="UTF-8").read()
    return html_skel % options

def log(message, level = DEBUG):
    """
    Logger
    """
    if level >= LOG_LEVEL:
        print(datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
              LEVEL[level],
              message,
              sep=' - ')

def load_main_page(contents: dict) -> str:
    status = contents['DATA']['STATUS']
    log(f'Le status du poele est {status}')
    if status in STATUS:
        status_label = STATUS[status]
    else:
        status_label = status
    
    bouton_off = ''
    bouton_on = ''
    # Gestion du bouton :
    if status in ON_STATUS:
        log('le poele est allumé.')
        bouton_on = 'style="display:none"'
    if status in OFF_STATUS:
        log('le poele est éteint.')
        bouton_off = 'style="display:none"'
    
    temperature1 = contents['DATA']['T1']
    temperature1 = '{:.1f}'.format(float(temperature1)).replace('.', ',') + '°'
    temperature2 = contents['DATA']['T2']
    temperature2 = '{:.1f}'.format(float(temperature2)).replace('.', ',') + '°'
    temperature3 = contents['DATA']['T3']
    temperature3 = '{:.1f}'.format(float(temperature3)).replace('.', ',') + '°'
    
    wanted_temperature = round(float(contents['DATA']['SETP']) * 10) / 10
    log(f'La températude demandée est {wanted_temperature}', INFO)

    power = contents['DATA']['PWR']

    locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')

    args = {
        'current_date': datetime.today().strftime('%A %d %B %Y - %H:%M:%S'),
        'status_label': status_label,
        'power': power,
        'bouton_off': bouton_off,
        'bouton_on': bouton_on,
        'temperature1': temperature1,
        'temperature2': temperature2,
        'temperature3': temperature3,
        'wanted_temperature': wanted_temperature
    }

    return charge_fichier_html('datas.html', args)