//var base_url = 'http://192.168.1.26:8080/poele/service/';
var base_url = 'https://88.174.46.231:16500/poele/service/';

var DEBUG = 0
var INFO = 1
var LOG_LEVEL = INFO

var STATUS = {
    0: 'Éteint',
    1: 'Arrêté',                          // Jamais vu
    2: "En cours d'allumage - Vérification",
    3: "En cours d'allumage - Chargement granulés",
    4: 'Allumage',
    5: 'Allumage - Contrôle combustion',
    6: 'Allumé',
    9: 'En veille - il fait assez chaud',
    10: "En cours d'extinction",
    11: 'Nettoyage',                      // Jamais vu
    12: 'Refroidissement',                // Jamais vu
    241: 'Erreur nettoyage',              // Jamais vu
    243: 'Erreur Grille',                 // Jamais vu
    244: 'NTC2 ALARM',                    // Jamais vu
    245: 'NTC3 ALARM',                    // Jamais vu
    247: 'Erreur Porte',                  // Jamais vu
    248: 'Erreur Dépression',             // Jamais vu
    249: 'NTC1 ALARM',                    // Jamais vu
    250: 'TC1 ALARM',                     // Jamais vu
    252: 'Erreur évacuation Fumée',       // Jamais vu
    253: "Il n'y a plus de granulés"
};

/*
Le poêle est allumé :
*/
var ON_STATUS = [2, 3, 4, 5, 6]
/*
Le poêle est éteint :
*/
var OFF_STATUS = [0, 10, 253]

const getDatas = async () => {
    log(DEBUG, new Date());
    var url = base_url + '?cmd=GET+ALLS';
    log(INFO, 'Exécution de la requête ' + url);
    const response = await fetch(url);
    const json = await response.json();
    log(DEBUG, json);

    var datas = json['DATA'];

    var wanted_temperature = datas['SETP'].toFixed(1);
    log(DEBUG, 'Temperature demandée : ' + wanted_temperature);
    setWantedTemperature(wanted_temperature);

    var power = datas['PWR'];
    setPower(power);

    var temperature_piece = datas['T1'].toFixed(1);
    setTemperaturePiece(temperature_piece);

    var pqt = datas['PQT'];
    setPelletQuantity(pqt);

    var status = parseInt(datas['STATUS']);
    log(DEBUG, 'Le status du poêle est : ' + status);
    setStatus(status);

    setCurrentDate();

    putOnOffButtons(status);
}

getDatas();
// Les données sont raffraichies toutes les 10 secondes.
setInterval(function() {
    getDatas()
}, 10000);

function allumePoele() {
    executeRequest("?cmd=CMD+ON");
}

function etteintPoele() {
    executeRequest("?cmd=CMD+OFF");
}

function putOnOffButtons(status) {
    document.getElementById("power_on").style.display = '';
    document.getElementById("power_off").style.display = '';
    if (ON_STATUS.includes(status)) {
        log(DEBUG, "Le poêle est allumé.");
        document.getElementById("power_on").style.display = 'none';
    }
    if (OFF_STATUS.includes(status)) {
        log(DEBUG, "Le poêle est éteint.");
        document.getElementById("power_off").style.display = 'none';
    }
}

function executeRequest(url) {
    url = base_url + url;
    log(INFO, 'Exécution de la requête : ' + url);
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", url, true); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;

}

function powerDown() {
    var power = parseInt(document.getElementById('power').innerHTML);
    if (power > 1) {
        power -= 1;
        document.getElementById('power').innerHTML = power;
        changePower(power);
    }
}

function powerUp() {
    var power = parseInt(document.getElementById('power').innerHTML);
    if (power < 5) {
        power += 1;
        document.getElementById('power').innerHTML = power;
        changePower(power);
    }
}

function changePower(power) {
    executeRequest("?cmd=SET+POWR+" + power);
}

function setPower(power) {
    document.getElementById('power').innerHTML = power;
}

function setWantedTemperature(newTemperature) {
    // MAJ du champ stockant la température demandée.
    var wanted_temperature_value = document.getElementById('wanted_temperature_value');
    if (wanted_temperature_value.innerHTML == '') {
        // Il s'agit du premier affichage :
        wanted_temperature_value.innerHTML = newTemperature;
    }
    if (wanted_temperature_value.innerHTML != newTemperature) {
        // La température demandée vient de changer.
        wanted_temperature_value.innerHTML = newTemperature;
        // Envoi de la requête au poêle.
        executeRequest("?cmd=SET+STPF+" + newTemperature);
    }
    // MAJ du "round slider".
    $("#wanted_temperature").roundSlider({
        sliderType: "min-range",
        value: parseFloat(wanted_temperature_value.innerHTML),
        min: 15,
        max: 25,
        step: 0.2,
        circleShape: "pie",
        width: 100,
        radius: 300,
        animation: false,
    });
}

function setTemperaturePiece(temperaturePiece) {
    var temperature_label = temperaturePiece.toString().replace('.', ',') + '°';
    document.getElementById('temperature_piece').innerHTML = temperature_label;
}

function setPelletQuantity(pqt) {
    document.getElementById('pqt').innerHTML = pqt;
}

function setStatus(status) {
    var status_label = STATUS[status];
    log(DEBUG, 'Le libellé du status est : ' + status_label);
    document.getElementById('status_label').innerHTML = status_label;
}

function setCurrentDate() {
    //On crée une date
    let date1 = new Date();

    let dateLocale = date1.toLocaleString('fr-FR',{
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'});

    document.getElementById('current_date').innerHTML = dateLocale;
}

function log(level, message) {
    if (level >= LOG_LEVEL) {
        let date1 = new Date();
        let dateLocale = date1.toLocaleString('fr-FR');
        console.log(dateLocale + ' - ' + message);
    }
}