#!/usr/bin/python
# -*- coding: utf-8 -*-

import cherrypy
import ihm_utils
import urllib.request
import json
import pathlib


class GestionPoele(object):
    """
    Cette classe permet de gérer l'appli Web de saisie des Questions.
    Le point d'entrée est l'URL http://localhost:8080/.
    """
    
    @cherrypy.expose
    def index(self, reload_period=20, base_url='http://176.173.252.138:160/'):
        """
        C'est la méthode appelée lorsque l'utilisateur exécute l'URL
        http://localhost:8080/
        C'est la page d'accueil de l'IHM Web.
        """
        return ihm_utils.charge_fichier_html("accueil.html")


    @cherrypy.expose
    @cherrypy.tools.json_out()
    def service(self, cmd):
        cmd = cmd.replace(' ', '+')
        ihm_utils.log(f'Paramètre du WS : {cmd}')
        url = f'http://192.168.1.13/cgi-bin/sendmsg.lua?cmd={cmd}'
        ihm_utils.log(f'Appel Web Service : {url}')
        contents = urllib.request.urlopen(url).read().decode('UTF-8')
        contents = json.loads(contents)
        # Pour ne pas avoir d'erreur : 'Blocage d’une requête multiorigines '.
        cherrypy.response.headers['Access-Control-Allow-Origin'] = '*'
        return contents

def configure_projet():
    """
    Fonction gérant la configuration du projet Web.
    """

    conf = {
        # Indique la racine de l'appli Web.
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': pathlib.Path(__file__).parent.resolve(),
            'tools.sessions.timeout': 60
        }
    }

    # Pour pouvoir accéder à l'appli depuis un autre PC du même réseau,
    # via http://IP:8090
    cherrypy.config.update({
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8080,
    })
    
    # Gestion de l'HTTPS.
    cherrypy.server.ssl_module = 'builtin'
    cherrypy.server.ssl_certificate = "/var/services/homes/benoit.domart/GIT/ihm-poele/cert.pem"
    cherrypy.server.ssl_private_key = "/var/services/homes/benoit.domart/GIT/ihm-poele/privkey.pem"

    # Vide le dossier temporaire.
    #vide_dossier_temporaire()

    return conf


def lance_ihm():
    """
    Méthode main permettant le démarrage de l'appli Web
    depuis la racine du projet.
    """

    conf = configure_projet()
    # Lance l'appli Web.
    cherrypy.quickstart(GestionPoele(), '/poele', conf)

# ################################
# C'est un des scripts principaux.
# Son exécution permet l'accès au module Web (http://localhost:8080)
# qui permet à l'enseignant de créer ses questions.
# ################################
if __name__ == '__main__':
    lance_ihm()
